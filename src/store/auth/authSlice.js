import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { setAuthorizationToken } from '../../services/baseApi';
import authApi from '../../services/authApi';
import userApi from '../../services/userApi';
const initialState = {
  token: '',
  user: '',
  loading: false,
};

//login
export const loginRequest = createAsyncThunk(
  'auth/loginRequest',
  async payload => {
    const response = await authApi.loginRequest(payload);
    return response;
  },
);

//get user profile
export const getUserProfileRequest = createAsyncThunk(
  'auth/getUserProfileRequest',
  async () => {
    const response = await userApi.getUserProfile();
    return response;
  },
);

//fetch invoices
export const fetchInvoicesRequest = createAsyncThunk(
  'auth/fetchInvoicesRequest',
  async ({ pageNum, pageSize, order }) => {
    const response = await userApi.fetchInvoices(pageNum, pageSize, order);
    return response;
  },
);

//create invoice
export const postInvoicesRequest = createAsyncThunk(
  'auth/postInvoicesRequest',
  async params => {
    const response = await userApi.postInvoices(params);
    return response;
  },
);

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    logoutSuccess: () => {
      return initialState;
    },
  },
  extraReducers: {
    [loginRequest.pending]: state => {
      state.loading = true;
    },
    [loginRequest.fulfilled]: (state, action) => {
      const { access_token } = action.payload;
      setAuthorizationToken(access_token);
      state.loading = false;
      state.token = access_token;
    },
    [loginRequest.rejected]: state => {
      state.loading = false;
    },
    [getUserProfileRequest.pending]: state => {
      state.loading = true;
    },
    [getUserProfileRequest.fulfilled]: (state, action) => {
      const { payload } = action;
      state.loading = false;
      state.user = payload.data;
    },
    [getUserProfileRequest.rejected]: state => {
      state.loading = false;
    },
  },
});

export const { logoutSuccess } = authSlice.actions;

export const auth = state => state.auth;

export default authSlice.reducer;
