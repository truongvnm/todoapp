import { combineReducers } from '@reduxjs/toolkit';
import authSlice from './auth/authSlice';

const reducers = combineReducers({
  auth: authSlice,
});
export default reducers;
