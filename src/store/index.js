import AsyncStorage from '@react-native-async-storage/async-storage';
import reducers from './reducers';
import Reactotron from 'reactotron-react-native';
import './../../reactotronConfig';
import { configureStore } from '@reduxjs/toolkit';
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist';
if (process.env.NODE_ENV === 'development') {
  console.log = Reactotron.log;
  // console.warn = Reactotron.warn;
  // console.error = Reactotron.error;
} else {
  console.log = () => {};
  console.warn = () => {};
  console.error = () => {};
}
const persistConfig = {
  key: 'root',
  version: 2,
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export const persistor = persistStore(store);
