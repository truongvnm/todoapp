import api from './baseApi';
import qs from 'qs';

async function loginRequest(params) {
  return api.post('token', qs.stringify(params), {
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
  });
}

export default {
  loginRequest,
};
