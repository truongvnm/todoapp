import api from './baseApi';

async function getUserProfile() {
  return api.get('membership-service/1.2.0/users/me');
}

async function fetchInvoices(page, size, order) {
  return api.get(
    `invoice-service/1.0.0/invoices?pageNum=${page}&pageSize=${size}&dateType=INVOICE_DATE&sortBy=CREATED_DATE&ordering=${order}`,
  );
}
async function postInvoices(params) {
  return api.post(`invoice-service/1.0.0/invoices`, params, {
    headers: { 'Content-Type': 'application/json', 'Operation-Mode': 'SYNC' },
  });
}

export default {
  getUserProfile,
  fetchInvoices,
  postInvoices,
};
