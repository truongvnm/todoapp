import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from '../routes/auth/login';
import { Routes } from '../routes/index';

const Stack = createStackNavigator();

const HomeNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{ headerShown: false }}
      initialRouteName={Routes.Login}>
      <Stack.Screen name={Routes.Login} component={LoginScreen} />
    </Stack.Navigator>
  );
};

export default HomeNavigator;
