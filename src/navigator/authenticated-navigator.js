import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../routes/home';
import CreateInvoiceScreen from '../routes/create-invoice';
import { Routes } from '../routes/index';

const Stack = createStackNavigator();

const HomeNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{ headerShown: false }}
      initialRouteName={Routes.Login}>
      <Stack.Screen name={Routes.Home} component={HomeScreen} />
      <Stack.Screen
        name={Routes.CreateInvoice}
        component={CreateInvoiceScreen}
      />
    </Stack.Navigator>
  );
};

export default HomeNavigator;
