import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import UnauthenticatedNavigator from './unauthenticated-navigator';
import AuthenticatedNavigator from './authenticated-navigator';
import { Routes } from '../routes';
import { useSelector } from 'react-redux';
const Stack = createStackNavigator();

const AppNavigator = () => {
  const auth = useSelector(state => state?.auth);
  const user = auth?.user;
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {!user ? (
          <Stack.Screen
            name={Routes.UnauthenticatedNavigator}
            component={UnauthenticatedNavigator}
            options={{ headerShown: false }}
          />
        ) : (
          <Stack.Screen
            name={Routes.AuthenticatedNavigator}
            component={AuthenticatedNavigator}
            options={{ headerShown: false }}
          />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
