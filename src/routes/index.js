export const Routes = {
  UnauthenticatedNavigator: 'UnauthenticatedNavigator',
  AuthenticatedNavigator: 'AuthenticatedNavigator',
  Login: 'Login',
  Home: 'Home',
  CreateInvoice: 'CreateInvoice',
};
