import React, { useState } from 'react';
import {
  SafeAreaView,
  Text,
  View,
  Keyboard,
  ActivityIndicator,
  Alert,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextInput, AppButton, ModalCalendarSelected } from '../../components';
import Images from '../../common/Images';
import userApi from '../../services/userApi';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import { setAuthorizationToken, setOrgToken } from '../../services/baseApi';
import { postInvoicesRequest } from '../../store/auth/authSlice';

const testData = {
  listOfInvoices: [
    {
      bankAccount: {
        bankId: '',
        sortCode: '09-01-01',
        accountNumber: '12345678',
        accountName: 'John Terry',
      },
      customer: {
        firstName: 'Nguyen',
        lastName: 'Dung 2',
        contact: {
          email: 'nguyendung2@101digital.io',
          mobileNumber: '+6597594971',
        },
        addresses: [
          {
            premise: 'CT11',
            countryCode: 'VN',
            postcode: '1000',
            county: 'hoangmai',
            city: 'hanoi',
          },
        ],
      },
      documents: [
        {
          documentId: '96ea7d60-89ed-4c3b-811c-d2c61f5feab2',
          documentName: 'Bill',
          documentUrl: 'http://url.com/#123',
        },
      ],
      invoiceReference: '#123456',
      invoiceNumber: 'TINV123456701',
      currency: 'GBP',
      invoiceDate: '2021-05-27',
      dueDate: '2021-06-04',
      description: 'Invoice is issued to Akila Jayasinghe',
      customFields: [
        {
          key: 'invoiceCustomField',
          value: 'value',
        },
      ],
      extensions: [
        {
          addDeduct: 'ADD',
          value: 10,
          type: 'PERCENTAGE',
          name: 'tax',
        },
        {
          addDeduct: 'DEDUCT',
          type: 'FIXED_VALUE',
          value: 10.0,
          name: 'discount',
        },
      ],
      items: [
        {
          itemReference: 'itemRef',
          description: 'Honda RC150',
          quantity: 1,
          rate: 1000,
          itemName: 'Honda Motor',
          itemUOM: 'KG',
          customFields: [
            {
              key: 'taxiationAndDiscounts_Name',
              value: 'VAT',
            },
          ],
          extensions: [
            {
              addDeduct: 'ADD',
              value: 10,
              type: 'FIXED_VALUE',
              name: 'tax',
            },
            {
              addDeduct: 'DEDUCT',
              value: 10,
              type: 'PERCENTAGE',
              name: 'tax',
            },
          ],
        },
      ],
    },
  ],
};
const CreateInvoice = () => {
  const navigation = useNavigation();
  const [isSubmit, setIsSubmit] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const auth = useSelector(state => state?.auth);
  const [modalCalendarVisible, setModalCalendarVisible] = useState(false);
  const [invoiceParams, setInvoiceParams] = useState({
    invoiceNumber: '',
    invoiceReference: '',
    description: '',
    invoiceDate: '',
  });
  const token = auth.token;
  const dispatch = useDispatch();

  const handleCreate = async () => {
    Keyboard.dismiss();
    setIsSubmit(true);
    if (validateDataLogin()) {
      setIsLoading(true);
      console.log(invoiceParams);
      const mergeObjValue = { ...testData.listOfInvoices[0], ...invoiceParams };
      console.log(mergeObjValue);
      testData.listOfInvoices[0] = mergeObjValue;
      console.log(testData);
      try {
        setAuthorizationToken(token);
        setOrgToken(auth.user.memberships[0].token);
        const result = await dispatch(postInvoicesRequest(testData));
        const { payload } = result;
        if (payload.status.code == '000000') {
          Alert.alert('Success', payload.status.message);
          setIsSubmit(false);
          setInvoiceParams({
            invoiceNumber: '',
            invoiceReference: '',
            description: '',
            invoiceDate: '',
          });
        }
        setIsLoading(false);
      } catch (error) {
        Alert.alert('Error', 'Add new invoice error');
        setIsLoading(false);
      }
    }
  };
  const validateDataLogin = () => {
    let isValid = true;
    const { invoiceNumber, invoiceReference, description, invoiceDate } =
      invoiceParams;
    if (!invoiceNumber || !invoiceReference || !description || !invoiceDate) {
      isValid = false;
    }
    return isValid;
  };
  const onSelectedDate = val => {
    setInvoiceParams(prevState => ({
      ...prevState,
      invoiceDate: val,
    }));
  };
  return (
    <SafeAreaView style={{ backgroundColor: '#FFF', flex: 1 }}>
      <KeyboardAwareScrollView
        enableOnAndroid={false}
        alwaysBounceVertical={true}
        showsVerticalScrollIndicator={false}
        bounces={false}>
        <View style={{ backgroundColor: '#FFF', flex: 1 }}>
          <View
            style={{
              height: 200,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: 22,
                fontWeight: '500',
                textTransform: 'uppercase',
              }}>
              Create Invoice
            </Text>
          </View>
          <View style={{ marginHorizontal: 16 }}>
            <TextInput
              placeholder="Enter Invoice Reference"
              onChangeText={value =>
                setInvoiceParams(prevState => ({
                  ...prevState,
                  invoiceReference: value,
                }))
              }
              returnKeyType="next"
              value={invoiceParams.invoiceReference}
              label="Invoice Reference"
              errorMessage={
                isSubmit && !invoiceParams.invoiceReference
                  ? `Invoice Reference is required`
                  : ''
              }
            />
            <TextInput
              placeholder="Enter Invoice Number"
              onChangeText={value =>
                setInvoiceParams(prevState => ({
                  ...prevState,
                  invoiceNumber: value,
                }))
              }
              returnKeyType="next"
              value={invoiceParams.invoiceNumber}
              label="Invoice Number"
              errorMessage={
                isSubmit && !invoiceParams.invoiceNumber
                  ? `Invoice Number is required`
                  : ''
              }
            />
            <TextInput
              placeholder="Enter Description"
              onChangeText={value =>
                setInvoiceParams(prevState => ({
                  ...prevState,
                  description: value,
                }))
              }
              numberOfLines={4}
              multiline={true}
              inputStyle={{ height: 60, marginVertical: 10 }}
              returnKeyType="next"
              value={invoiceParams.description}
              label="Description"
              errorMessage={
                isSubmit && !invoiceParams.description
                  ? `Description is required`
                  : ''
              }
            />
            <AppButton onPress={() => setModalCalendarVisible(true)}>
              <TextInput
                placeholder="Select Invoice Date"
                onChangeText={value =>
                  setInvoiceParams(prevState => ({
                    ...prevState,
                    invoiceDate: value,
                  }))
                }
                numberOfLines={4}
                multiline={true}
                inputStyle={{ height: 60, marginVertical: 10 }}
                returnKeyType="next"
                value={invoiceParams.invoiceDate}
                label="Invoice Date"
                editable={false}
                errorMessage={
                  isSubmit && !invoiceParams.invoiceDate
                    ? `Invoice Date is required`
                    : ''
                }
              />
            </AppButton>
            <ModalCalendarSelected
              setModalVisible={() =>
                setModalCalendarVisible(!modalCalendarVisible)
              }
              modalVisible={modalCalendarVisible}
              setSelected={onSelectedDate}
              value={invoiceParams.invoiceDate}
            />
          </View>
          <AppButton
            onPress={handleCreate}
            style={{
              backgroundColor: '#00AB9D',
              marginHorizontal: 16,
              borderRadius: 10,
              alignItems: 'center',
              justifyContent: 'center',
              height: 48,
              marginVertical: 20,
            }}>
            <Text
              style={{ fontSize: 16, fontWeight: '700', color: '#FFF' }}
              appStyle="size16 w700 white">
              Create
            </Text>
          </AppButton>
        </View>
      </KeyboardAwareScrollView>
      {isLoading && (
        <View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size="large" color="#333" />
        </View>
      )}
    </SafeAreaView>
  );
};

export default CreateInvoice;
