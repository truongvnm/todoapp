import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StatusBar,
  Text,
  View,
  Image,
  Keyboard,
  ActivityIndicator,
  Alert,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextInput, AppButton } from '../../../components';
import Images from '../../../common/Images';
import { isEmail } from '../../../utils/validate';
import { setAuthorizationToken } from '../../../services/baseApi';
const { StaffIcon, PasswordIcon, Logo } = Images;
import appConfigs from '../../../common/appConfigs';
import { useDispatch } from 'react-redux';
import {
  loginRequest,
  logoutSuccess,
  getUserProfileRequest,
} from '../../../store/auth/authSlice';

const Login = () => {
  const [email, setEmail] = useState('dung+octopus4@101digital.io');
  const [password, setPassword] = useState('Abc@123456');
  const [isSubmit, setIsSubmit] = useState(false);
  const [errorEmail, setErrorEmail] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();
  const getUserProfile = async access_token => {
    try {
      dispatch(getUserProfileRequest(access_token));
    } catch (error) {}
  };
  // useEffect(()=>{dispatch(logoutSuccess())},[])
  const handleLogin = async () => {
    Keyboard.dismiss();
    setIsSubmit(true);
    if (validateDataLogin()) {
      setIsLoading(true);
      const payload = {
        username: email.trim(),
        password: password,
        ...appConfigs.commonConfigs,
      };
      try {
        const result = await dispatch(loginRequest(payload));
        const { id_token, access_token } = result.payload;
        if (id_token) {
          setAuthorizationToken(access_token);
          await getUserProfile(access_token);
        }
        setIsLoading(false);
      } catch (error) {
        Alert.alert('Error', 'No user found');
        setIsLoading(false);
      }
    }
  };
  const validateDataLogin = () => {
    let isValid = true;
    let errorEmailValue = '';
    if (!email || !password) {
      isValid = false;
    }
    if (!isEmail(email)) {
      isValid = false;
      errorEmailValue = 'This email is invalid';
    }
    setErrorEmail(errorEmailValue);
    return isValid;
  };
  return (
    <SafeAreaView style={{ backgroundColor: '#FFF', flex: 1 }}>
      <StatusBar barStyle="dark-content" />
      <KeyboardAwareScrollView
        enableOnAndroid={false}
        alwaysBounceVertical={true}
        showsVerticalScrollIndicator={false}
        bounces={false}>
        <View style={{ backgroundColor: '#FFF', flex: 1 }}>
          <View
            style={{
              height: 200,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              style={{
                alignItems: 'center',
                justifyContent: 'center',
              }}
              source={Logo}
            />
          </View>
          <View style={{ marginHorizontal: 16 }}>
            <TextInput
              placeholder="Enter Email"
              onChangeText={setEmail}
              headIcon={<StaffIcon />}
              returnKeyType="next"
              value={email}
              label="Email"
              errorMessage={
                isSubmit && !email
                  ? `Email is required`
                  : errorEmail
                  ? errorEmail
                  : ''
              }
            />
            <TextInput
              placeholder="Enter Password"
              onChangeText={setPassword}
              headIcon={<PasswordIcon />}
              returnKeyType="go"
              value={password}
              label="Password"
              errorMessage={isSubmit && !password ? `Password is required` : ''}
              secure
            />
          </View>
          <AppButton
            onPress={handleLogin}
            appStyle={{
              backgroundColor: '#00AB9D',
              marginHorizontal: 16,
              borderRadius: 10,
              alignItems: 'center',
              justifyContent: 'center',
              height: 48,
            }}
            style={
              !email || !password
                ? { backgroundColor: 'rgba(0,171,157,0.4)' }
                : { backgroundColor: '#00AB9D' }
            }>
            <Text
              style={{ fontSize: 16, fontWeight: '700', color: '#FFF' }}
              appStyle="size16 w700 white">
              Login
            </Text>
          </AppButton>
        </View>
      </KeyboardAwareScrollView>
      {isLoading && (
        <View
          style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size="large" color="#333" />
        </View>
      )}
    </SafeAreaView>
  );
};

export default Login;
