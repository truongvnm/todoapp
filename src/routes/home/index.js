import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { Routes } from '../../routes';
import { setAuthorizationToken, setOrgToken } from '../../services/baseApi';
import { AppButton } from '../../components';
import { useNavigation } from '@react-navigation/native';
import { logoutSuccess } from '../../store/auth/authSlice';
import { fetchInvoicesRequest } from '../../store/auth/authSlice';

const pageNum = 1;
const pageSize = 10;

const Home = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const auth = useSelector(state => state?.auth);
  const [invoices, setInvoices] = useState([]);
  const [paging, setPaging] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [order, setOrder] = useState('ASCENDING');
  const token = auth.token;

  const fetchInvoices = async (pageNum, pageSize) => {
    try {
      setOrgToken(auth.user.memberships[0].token);
      const result = await dispatch(
        fetchInvoicesRequest({ pageNum, pageSize, order }),
      );
      const { payload } = result;
      setInvoices([...invoices, ...payload.data]);
      setPaging(payload.paging);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    setAuthorizationToken(token);
    setIsLoading(true);
    fetchInvoices(pageNum, pageSize);
  }, [order]);

  //logout
  const logout = () => {
    dispatch(logoutSuccess());
  };

  //order
  const onOrder = () => {
    setOrder(order == 'ASCENDING' ? 'DESCENDING' : 'ASCENDING');
    setInvoices([]);
  };

  const renderInvoive = ({ item }) => {
    return (
      <View
        style={{
          borderLeftColor: '#43A047',
          borderLeftWidth: 3,
          flexDirection: 'row',
          alignItems: 'center',
          marginTop: 10,
          borderRadius: 2,
        }}>
        <View style={{ flex: 1, padding: 10 }}>
          <View style={{}}>
            <Text style={{ color: '#333' }}>{item.invoiceNumber}</Text>
          </View>
          <View stylye={{}}>
            <View
              style={{ flexDirection: 'row', justifyContent: 'space-between' }}
              appStyle="row jusB">
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <Text
                  style={{ fontSize: 12, color: '#A7A7A7', lineHeight: 24 }}>
                  Total Amount:
                </Text>
                <Text
                  style={{ fontSize: 12, color: '#A7A7A7', lineHeight: 24 }}>
                  {item.totalAmount}
                </Text>
              </View>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                <Text
                  style={{ fontSize: 12, color: '#A7A7A7', lineHeight: 24 }}>
                  Total Tax:
                </Text>
                <Text
                  style={{ fontSize: 12, color: '#A7A7A7', lineHeight: 24 }}>
                  {item.totalTax}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  };
  const footerLoading = () => (
    <>
      <View style={{ marginBottom: 30 }} />
      {isLoading ? (
        <View style={{ paddingVertical: 20 }}>
          <ActivityIndicator color="#333" size="large" />
        </View>
      ) : (
        <View />
      )}
    </>
  );
  const onEndReached = () => {
    if (paging.totalRecords < paging.pageNumber * pageSize) return;
    if (!isLoading) {
      setIsLoading(true);
      fetchInvoices(paging.pageNumber + 1, pageSize);
    }
  };
  const renderHeader = () => {
    return (
      <View>
        <View
          style={{
            margin: 16,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <AppButton
              onPress={() => navigation.navigate(Routes.CreateInvoice)}>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: '500',
                }}>
                Add New
              </Text>
            </AppButton>
          </View>
          <View style={{ flex: 0.6, alignItems: 'center' }}>
            <Text
              style={{
                fontSize: 20,
                textTransform: 'uppercase',
                fontWeight: '500',
              }}>
              Invoices
            </Text>
          </View>
          <View>
            <AppButton onPress={logout}>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: '500',
                }}>
                Logout
              </Text>
            </AppButton>
          </View>
        </View>
        <View
          style={{
            margin: 16,
            flexDirection: 'row',
          }}>
          <AppButton onPress={onOrder}>
            <Text
              style={{
                fontSize: 16,
                fontWeight: '500',
              }}>
              {`Order by: ${order} ↑↓`}
            </Text>
          </AppButton>
        </View>
      </View>
    );
  };
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: '#FFF' }}>
      {renderHeader()}
      <FlatList
        data={invoices}
        style={{ flex: 1, marginHorizontal: 16 }}
        initialNumToRender={10}
        maxToRenderPerBatch={10}
        renderItem={renderInvoive}
        keyExtractor={(item, index) => `${item.invoiceId}${index}`}
        ListFooterComponent={footerLoading}
        onEndReached={onEndReached}
        onEndReachedThreshold={0.5}
        showsVerticalScrollIndicator={false}
      />
    </SafeAreaView>
  );
};
export default Home;
