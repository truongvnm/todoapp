import React, { useState } from 'react';
import AppButton from '../app-button';
import { Modal, StyleSheet, Text, View } from 'react-native';
import { Calendar } from 'react-native-calendars';
import moment from 'moment';

const INITIAL_DATE = moment().format('YYYY-MM-DD');
const ModalCalendarSelected = props => {
  const { modalVisible, setModalVisible, setSelected, value, changeYear } =
    props;
  const [dateSelected, setDateSelected] = useState({});
  const [selectedDay, setSelectedDay] = useState(value);
  const [currentYear, setCurrentYear] = useState(
    moment(INITIAL_DATE).format('YYYY'),
  );

  const onDayPress = day => {
    setDateSelected({
      [day.dateString]: { selected: true, selectedColor: '#00AB9D' },
    });
    setSelectedDay(day.dateString);
  };

  const onPressSubmit = () => {
    if (selectedDay) {
      setSelected(selectedDay);
    }
    setModalVisible(false);
  };
  const onPressCancel = () => {
    if (changeYear) {
      if (value) {
        setCurrentYear(moment(value, 'DD/MM/YYYY').format('YYYY'));
      } else {
        setCurrentYear(moment(INITIAL_DATE).format('YYYY'));
      }
    }
    setModalVisible(false);
  };
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible();
      }}>
      <View
        style={{
          backgroundColor: 'rgba(0,0,0,0.5)',
          flex: 1,
          justifyContent: 'center',
        }}>
        <View style={styles.modalView}>
          <Calendar
            current={value ? value : INITIAL_DATE}
            onDayPress={onDayPress}
            // customHeader={CustomHeader}
            theme={{
              todayTextColor: '#00AB9D',
              textDayFontSize: 15,
              textDayFontWeight: '400',
              selectedDayBackgroundColor: '#00adf5',
              selectedDayTextColor: '#ffffff',
            }}
            hideExtraDays={true}
            markedDates={dateSelected}
          />
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-end',
              marginTop: 15,
            }}>
            <AppButton
              style={{
                paddingHorizontal: 10,
                paddingVertical: 5,
                marginRight: 30,
              }}
              onPress={onPressCancel}>
              <Text style={{ color: 'blue', fontWeight: '600', fontSize: 16 }}>
                CANCEL
              </Text>
            </AppButton>
            <AppButton
              style={{ paddingHorizontal: 10, paddingVertical: 5 }}
              onPress={onPressSubmit}>
              <Text style={{ color: 'blue', fontWeight: '600', fontSize: 16 }}>
                OK
              </Text>
            </AppButton>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalView: {
    marginHorizontal: 16,
    marginVertical: 0,
    backgroundColor: 'white',
    borderRadius: 4,
    padding: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});

export default ModalCalendarSelected;
