import React from 'react';
import { TouchableOpacity } from 'react-native';

const AppButton = props => {
  const {
    children,
    style = {},
    onPressIn = () => {},
    onPress = () => {},
    appStyle,
    activeOpacity,
    disabled,
    ...restProps
  } = props;
  return (
    <TouchableOpacity
      activeOpacity={activeOpacity || 0.7}
      onPress={onPress}
      disabled={disabled}
      onPressIn={onPressIn}
      {...restProps}
      style={[appStyle, style]}>
      {children}
    </TouchableOpacity>
  );
};

export default AppButton;
