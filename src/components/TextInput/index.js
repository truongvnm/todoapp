import React, { useState } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import Images from '../../common/Images';

const { ErrorIcon } = Images;

const Input = props => {
  const {
    value,
    secureTextEntry,
    keyboardType,
    onChangeText,
    headIcon,
    label,
    errorMessage,
    secure,
    editable = true,
    containerStyle,
  } = props;
  const [securePassword, setSecurePassword] = useState(true);

  const secureTextEntryObj = {};
  if (secure && securePassword) {
    secureTextEntryObj.secureTextEntry = true;
  } else {
    secureTextEntryObj.secureTextEntry = false;
  }
  return (
    <View
      appStyle=""
      style={[
        { marginBottom: errorMessage ? 12 : 24 },
        containerStyle && containerStyle,
      ]}>
      {!!label && (
        <View style={{ marginBottom: 8 }}>
          <Text style={{ fontSize: 14, color: '#2F2F2F' }}>{label}</Text>
        </View>
      )}
      <View
        style={[
          styles.container,
          errorMessage && { borderWidth: 2, borderColor: '#EF5350' },
          !editable && { backgroundColor: '#FFF' },
        ]}>
        {headIcon && (
          <View
            style={{
              justifyContent: 'center',
              marginRight: 14,
              alignItems: 'center',
              width: 20,
              height: 30,
            }}>
            {headIcon}
          </View>
        )}
        <View style={{ flex: 1 }}>
          {editable ? (
            <TextInput
              style={[styles.textinput, props.inputStyle]}
              placeholderTextColor="#C0C0C0"
              autoCorrect={false}
              underlineColorAndroid="transparent"
              value={value}
              onChangeText={onChangeText}
              keyboardType={keyboardType}
              secureTextEntry={secureTextEntry}
              editable={editable}
              {...props}
              {...secureTextEntryObj}
            />
          ) : (
            <View
              style={{ paddingVertical: 5, flex: 1, justifyContent: 'center' }}>
              <Text
                style={{
                  fontSize: 14,
                  color: value ? '#2F2F2F' : '#C0C0C0',
                  lineHeight: 16,
                }}>
                {value ? value : `Select ${label}`}
              </Text>
            </View>
          )}
        </View>
        {secure && (
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => setSecurePassword(prevState => !prevState)}>
            <View
              style={{ height: 30, marginLeft: 5, justifyContent: 'center' }}
              height={30}>
              <Images.EyeIcon color="#78909C" />
            </View>
          </TouchableOpacity>
        )}
      </View>
      {!!errorMessage && (
        <View
          style={{
            flexDirection: 'row',
            marginTop: 6,
            alignItems: 'center',
            minHeight: 18,
          }}>
          <View style={{ marginRight: 16 }}>
            <ErrorIcon />
          </View>
          <View style={{ flexDirection: 'row', flex: 1 }}>
            <Text style={{ fontSize: 14, color: '#f44336' }}>
              {errorMessage}
            </Text>
          </View>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#C0C0C0',
    borderWidth: 1,
    paddingLeft: 14,
    paddingRight: 12,
    borderRadius: 4,
    backgroundColor: '#FFF',
    minHeight: 56,
  },
  textinput: {
    flex: 1,
    textAlign: 'left',
    fontSize: 14,
  },
});

export default Input;
