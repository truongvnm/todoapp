import EyeIcon from '../assets/svg/eyeIcon';
import ErrorIcon from '../assets/svg/errorIcon';
import StaffIcon from '../assets/svg/staffIcon';
import PasswordIcon from '../assets/svg/lockIcon';

const Images = {
  EyeIcon,
  ErrorIcon,
  StaffIcon,
  PasswordIcon,
  Logo: require('../assets/logo.png'),
};
export default Images;
