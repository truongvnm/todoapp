import * as React from 'react';
import Svg, { G, Path, Circle } from 'react-native-svg';

const StaffIcon = props => (
  <Svg xmlns="http://www.w3.org/2000/svg" width={18} height={20} {...props}>
    <G
      transform="translate(1 1)"
      fill="none"
      stroke="#78909c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={2}>
      <Path data-name="Path 24" d="M16 18v-2a4 4 0 0 0-4-4H4a4 4 0 0 0-4 4v2" />
      <Circle
        data-name="Ellipse 5"
        cx={4}
        cy={4}
        r={4}
        transform="translate(4)"
      />
    </G>
  </Svg>
);

export default StaffIcon;
