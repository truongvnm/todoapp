import * as React from 'react';
import Svg, { G, Circle, Path } from 'react-native-svg';

const ErrorIcon = props => (
  <Svg xmlns="http://www.w3.org/2000/svg" width={18} height={18} {...props}>
    <G
      transform="translate(-1 -1)"
      fill="none"
      stroke="#ef5350"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={2}>
      <Circle
        data-name="Ellipse 582"
        cx={8}
        cy={8}
        r={8}
        transform="translate(2 2)"
      />
      <Path data-name="Line 241" d="M10 6.5v4" />
      <Path data-name="Line 242" d="M9.999 13.2h.01" />
    </G>
  </Svg>
);

export default ErrorIcon;
