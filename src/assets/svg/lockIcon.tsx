import * as React from 'react';
import Svg, { G, Rect, Path } from 'react-native-svg';

const lockIcon1 = props => (
  <Svg xmlns="http://www.w3.org/2000/svg" width={20} height={22} {...props}>
    <G
      transform="translate(1 1)"
      fill="none"
      stroke="#78909c"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeWidth={2}>
      <Rect
        data-name="Rectangle 7"
        width={18}
        height={11}
        rx={2}
        transform="translate(0 9)"
      />
      <Path data-name="Path 25" d="M4 9V5a5 5 0 1 1 10 0v4" />
    </G>
  </Svg>
);

export default lockIcon1;
